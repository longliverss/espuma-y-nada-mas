---
layout: post
title: Mostly hypotetical mountains
date: 2017-10-22
description: 
image: /genImag/this is_what_we_call.jpg
author: Llrss
tags: 
  - long
  - live
  - rss
---
In final, sun-filled days of the “Done-In-The-Sun Summer Genocide Concert Series” on planet Earth, in which the Bed Bugs financed a mass migration of those counted among the living to blood-retrieval executive sweat-shops in what was recently Hell proper, the Earth basked and spun silently, save for the austere purr of the occasional Jet-Pack. After a while, the mountains sifted any themed-debris that lay between them down into the bowels of the valley and on out into space, sighing audibly all the while.

The Bed Bugs had decided to undertake a massive renovation of human consciousness, which was to take place inside thousands of weapons-grade modules mounted in deep space. The whole project was funded by the wholesale absorbing of the Earth’s economy by the Bed Bug’s daring to inject patented products and advertising that served intensely physiological human needs into a market dominated by worthless intellectual properties. After making a profoundly opaque deal with Satan to purchase Hell and the ‘Hell’s Restaurant’ franchise, which served a drug compound rapidly causing a free-market inspired gold-rush to Hell, the Bed Bugs had enough of a blood supply at their disposal to create the vital metadata reduction of DNA the human consciousness program required to operate.

![Placeholder](/genImag/this is_what_we_call.jpg)

 The Bed Bugs had spent their whole existence observing from the fairly intimate vantage point of mattresses the way our global civilization operates, and decided that for humanity to have one class of people who toil without dignity so that the other class can explore matters of consciousness and awareness-of-the-potentiality-for-the-existence-of-wishful-thinking-inspired-alternate-realities-related issues at their leisure, would be a compassionate and culturally sensitive way to gradually transition them naturally into the next level of their evolution.

Oblivious of their recent non-consensual conception, their unlikely, but undeniably exquisite, Reality-Based, snowflake-shaped physical form; and the fact that each one of them was an absolute miracle of Bed Bug ingenuity; the people who had been created by the Bed Bugs lived simply, housed solely within the physical representations of their internal projections and the visible, cumulative manifestation of very old, totally unrelated, nomadic space debris from a previous age. Each engineered consciousness was it’s own master, and lived without need, since no one lacked anything that everyone else did not also lack. The things that did exist independently of them, that no one could tell you when or how they got there, or if they had been made or fell from the sky, or what; were treated with reverence and awe.

![Placeholder](/genImag/this is_what_we_call.jpg)

 “Smell that?” he said alertly.

“What?” he whispered brusquely, eyes darting with able-bodied apprehension.

“Frozen Pizza close by. Smells like it’s wafting from that clearing by the river.”

His companion put his nose to the air. “Yes! I can smell it! It’s incredible.” He put a hand slowly on the Panini grill hanging from his belt.

“What are you doing?” he hissed. “That Frozen Pizza is a free agent! You cannot take him against his will anymore than you can just pluck a patent from out of the sky or put a river in the microwave! We must speak to it.”

The two approached the Frozen Pizza respectfully, so as not to surprise it.

###### Credits: [https://mostlyhypotheticalmountains.blogspot.com/](https://mostlyhypotheticalmountains.blogspot.com/)
