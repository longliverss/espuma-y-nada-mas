# Espuma (ES)

Esta es una modificación del tema [Barber](https://github.com/samesies/barber-jekyll).

El tema no se aleja mucho del original, por lo que recomendamos leer las instrucciones en el [repo original](https://github.com/samesies/barber-jekyll). Sin embargo, hay un par de cambios que debemos mencionar:

## Funcionamiento

Este tema incluye un plugin que se encarga de generar una página por cada imagen encontrada en un directorio predefinido en _config.yml_. Lo único que tienes que hacer es agregar imágenes al directorio en cuestión, correr `bundle exec jekyll build` y automáticamente se generarán las páginas para las imágenes añadidas.

## Configuración inicial

Abre _config.yml_ y en las últimas líneas encontrarás las configuraciones del plugin. Cambia estas variables según lo necesites.

```
# imagPageGen plugin settings
images_folder: "/genImag" # ruta relativa path del directorio donde guardas las imágenes
images_collection: "coleccion1" # Nombre de la colección a la que pertenecen las páginas autogeneradas. Recuerda añadir este nombre también a pagination.collection en config.yml (línea 49)
images_ordinal_title: "Imagen #" # título dado a cada página generada, indicando su número ordinal. 
images_filename_template: "Filename_" # si las imágenes tienen en su nombre de archivo la fecha en formato YYYYMMDD, añade acá los acaracteres que preceden dicha fecha. 
```

No olvides en la línea 49 en las configuraciones de paginación agregar el nombre que le diste a la colección de páginas autogeneradas.

## Agrega imágenes

Agrega las imágenes que desees al directorio configurado para ello.

## Construye tu sitio!

después, sólo resta correr `bundle exec jekyll build` y Jekyll se encargará del resto.

## Deployment

### Github Pages

El tema usa plugins personalizados, por lo que para evitar errores podrías correr `bundle exec jekyll build` o `npm run build` y añadir manualmente los contenidos de `_site` al branch `gh-pages`.

### Netlify

Puedes enlazar el repo a [Netlify](https://www.netlify.com/). Netlify se encargará de armar el sitio cada vez que haya un commit en el repo. Sin embargo, encontramos errores asociados a sass (el error indicaba `npm ERR! path /opt/build/repo/node_modules/node-sass`). Logramos resolverlo cambiando la versión de node que se estaba usando. 

Para ello:

- En las configuraciones del proyecto en Netlify, busca `build and deploy --> Environment --> Environment variables`
- Añade una variable llamada `NODE_VERSION` y agrega como valor a ella la versión de node que deseas utilizar (en nuestro caso, `14`)


# Foam (ENG)

This is a fork of the great [Barber theme](https://github.com/samesies/barber-jekyll)

The theme is pretty similar to the original one, so we recommend to read the instructions on the [barber repo](https://github.com/samesies/barber-jekyll). There are a few changes, though:

This theme includes a custom plugin that auto-generates pages for each image found in a specific folder (which is defined in _config.yml_). Therefore, the only thing you need to do (besides personalizing the theme according to your needs) is to add the images to the folder and build the site.

## Update settings

Open _config.yml_. In the end of the file you'll find the plugin settings:

```
# imagPageGen plugin settings
images_folder: "/genImag" # relative path of the folder where the images are stored
images_collection: "coleccion1" # name of the collection. Remember to add the same collection name to pagination.collection in this very same file
images_ordinal_title: "Imagen #" # title given to each page taking into account its order
images_filename_template: "Filename_" # if your image filenames contain the date in format YYYYMMDD, add here the characters preceding it. 
```

Don't forget to update line 49 with the same name you gave the collection of pages generated.

## Add the images

Add your images to the folder set in _config.yml_.

## Build and deploy!

Run `bundle exec jekyll build`. Jekyll will take care of the rest.

## Deployment

### Github Pages

Since the theme uses custom plugins, to deploy the site on Gitgub Pages run `bundle exec jekyll build` or `npm run build` and manually add the contents of the `_site` folder to the `gh-pages` branch.

### Netlify

You can link your repo to [Netlify](https://www.netlify.com/). Netlify will build the site with each commit. 

We found errors related to sass (`npm ERR! path /opt/build/repo/node_modules/node-sass`). To fix them, change the node version used in the deployment:

- In the Netlify project settings, seach for `build and deploy --> Environment --> Environment variables`
- Add a new variable called `NODE_VERSION`
- The value of this variable is the version of node you wanna use. We found the errors were fixed when we used `14`

## Commits:

__001 - Inicial__

__002 - Borrar contenido__

  - Todos los archivos en /Posts
	- Todas las imágenes en assets/images
	- styleguide.html
	- subscribe.html
  - datos varios en config.yml

__003 - New PageGenPlugin__

  - Configs personalizadas para el plugin en config.yml
  - layout de imagen de imagen generada
  - plugin.rb para generar páginas de colección determinada por el usuario a partir de un directorio lleno de imágenes.
  - test images
  - indice rudimentario en index.html

__004 -Autopages_collection__
 
  - config.yml collections paginator on with list of collections to be included
  - config.yml paginator sorting order cambiado de date a title, pues si había publicaciones sin fecha, se rompía.
  - layout de página de colecciones autogenerada
  - indice rudimentario en index.html eliminado
  - cambios en include de post-card para cubrir tanto publicaciones autogeneradas de imágenes como posts
  - nuevo post de prueba
  - nombre de archivo de imagen modificado para probar espacios en filenames
  
__005 -Rss__

  -config.yml social links (rss missing)
  -feed.xml actualizado. Ahora incluye las imágenes en vez del (inexistente) contenido del post.

__006 -Final touches__

- Toques finales en config.yml
- Styleguide añadida
- Cambiamos posición y tamaño de las fechas en el layout de las image pages.
- Readme updated.

__007 -Links__

- broken links fixed.

__008 -icons__

- Icons updated.